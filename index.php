<?php include 'config/config.php'; ?>

<!doctype html>  

<!--[if lt IE 7 ]>             <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>                <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>                <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>                <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	
	<!-- force latest IE rendering engine (even in intranet) and Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Computing Services - University of Liverpool</title>
	<meta name="Title" content="" />
	<meta name="Author" content="" />
	<meta name="Creator" content="The_CMS_Team_Owner" />
	<meta name="Contributor" content="Unknown" />
	<meta name="Publisher" content="University of Liverpool" />
	<!--keywords-->
	<meta name="Description" content="" />
	<!--date--><meta name="Date" content="Tue, Feb 14, 2012" />

	<!--last updated--><meta name="Last updated" content="Thu, Sep 20, 2012" />

	<meta name="Type" content="University of Liverpool Web Site" />
	<meta name="Format" content="text/html" />
	<meta name="Rights" content="Copyright University of Liverpool" />
	<meta name="Identifier" content="http://www.liv.ac.uk/" />
	<meta name="Language" content="en-uk" />
	
	<!--  Mobile viewport optimized: j.mp/bplateviewport  -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- favicon.ico & apple-touch-icon.png  -->
	<link rel="shortcut icon" href="http://www.liv.ac.uk/images/favicon.ico">
	<link rel="apple-touch-icon" href="http://www.liv.ac.uk/images/apple-touch-icon.png">
	
	<!-- CSS : implied media="all" -->

	<!-- HTML5 css reset, master style sheet, print, custom (if included), and web fonts. All hidden from IE6, which gets a universal style sheet -->

    <!--[if ! lte IE 6]><!-->
	<link rel="stylesheet" href="http://csdsite.liv.ac.uk/css/csd-responsive.css" media="screen, projection">
    <!--<![endif]-->
    
    <link rel="stylesheet" type="text/css" href="http://csdsite.liv.ac.uk/css/print.css" media="print"/>
	
    <!-- web fonts licencing notice -->
    <!--
	/*
	* MyFonts Webfont Build ID 849783, 2011-07-11T07:10:59-0400
	* 
	* The fonts listed in this notice are subject to the End User License
	* Agreement(s) entered into by the website owner. All other parties are 
	* explicitly restricted from using the Licensed Webfonts(s).
	* 
	* You may obtain a valid license at the URLs below.
	* 
	* Webfont: Swiss 721 Light
	* URL: http://new.myfonts.com/fonts/bitstream/swiss-721/light/
	* Foundry: Bitstream
	* Copyright: Copyright 1990-2000 Bitstream Inc.  All rights reserved.
	* License: http://www.myfonts.com/viewlicense?1056
	* Licensed pageviews: 1,000,000/month
	* CSS font-family: Swiss721GreekBT-Light
	* CSS font-weight: normal
	* 
	* Webfont: Swiss 721 Heavy
	* URL: http://new.myfonts.com/fonts/bitstream/swiss-721/heavy/
	* Foundry: Bitstream
	* Copyright: Copyright 1990-1999 as an unpublished work by Bitstream Inc.  All rights reserved.  Confidential.
	* License: http://www.myfonts.com/viewlicense?1056
	* Licensed pageviews: 100,000/month
	* CSS font-family: Swiss721BT-Heavy
	* CSS font-weight: normal
	* 
	* Webfont: Swiss 721 Heavy Italic
	* URL: http://new.myfonts.com/fonts/bitstream/swiss-721/heavy-italic/
	* Foundry: Bitstream
	* Copyright: Copyright 1990-1999 as an unpublished work by Bitstream Inc.  All rights reserved.  Confidential.
	* License: http://www.myfonts.com/viewlicense?1056
	* Licensed pageviews: 100,000/month
	* CSS font-family: Swiss721BT-HeavyItalic
	* CSS font-weight: normal
	* 
	* (c) 2011 Bitstream Inc
	*/
	-->

    <!-- universal style sheet for IE6 -->
    <!--[if lte IE 6]>
    <link rel="stylesheet" href="http://csdsite.liv.ac.uk/css/ie6.css" media="screen, projection">
    <![endif]-->
	
    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
    Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
    For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://csdsite.liv.ac.uk/js/libs/modernizr-2.0.6.min.js"></script>
  
</head>

<body class="admin responsive" id="computing-services">
  <div id="container">
    <header id="masthead" role="banner">
        <a href="#computing-services-home" title="Skip Navigation" id="skip-nav" class="visuallyhidden">Skip navigation</a>
        <h1>
            <a id="uni-home-link" href="http://www.liv.ac.uk" title="University of Liverpool Home">University of Liverpool - </a>
            <span id="dept-home-link"><a href="#">Computing Services</a></span>
        </h1>
        
        <div id="global-and-search">
            <nav id="global-nav">
                <ul>
                    <li><a href="http://www.liv.ac.uk">University Home</a></li>
                    <li><a href="http://www.liv.ac.uk/a-z/">A-to-Z</a></li>
                    <li><a href="https://staff.liv.ac.uk">Staff</a></li>
                    <li><a href="http://www.liv.ac.uk/students/index.htm">Students</a></li>
                </ul>
            </nav>
            <hr/>
        
            <form role="search" name="gs" action="http://www.liv.ac.uk/search/results/index.php" method="get" id="search">
                <label for="query" class="visuallyhidden">Search the University</label>
                <input type="text" placeholder="search" class="search-input" value="" maxlength="255" id="query" size="25" name="query">
                <input type="submit" class="search-button ready" value="Search" name="btnG">
                <input type="hidden" value="1" name="start_rank">
                <input type="hidden" value="1" name="page">
                <input type="hidden" value="main-collection" name="collection">
            </form>
        </div>
    </header>
	
    <hr>
    	
	<section id="computing-services-home">
    
        <hr class="hang-line">
 
    	<!-- promo spot begins -->
        <div id="promo-spot">
        	<div class="feature dark">
                <h1 class="visuallyhidden">Quickstart - your guide to IT services</h1>
                <img src="//csdsite.liv.ac.uk/img/quickstart.jpg" alt="Quickstart - your guide to IT services"/>
                <div class="news-summary">
                    <p>Our Quickstart pages have all the information you'll need to activate your University computing account, email account, and more...</p>
                    <a href="http://www.liv.ac.uk/csd/quickstart/" class="call-to-action">visit quickstart</a>
                </div>
            </div>
        </div>
        <!-- promo spot ends -->

        <!-- hang line -->
        <hr class="hang-line" id="promo-spot-hang-line">

        <!-- quick links begins -->
        <nav id="quick-links" class="computing-home-module">
        	<h1>Quick links</h1>
            <ul>
            	<li id="quick-links-password">
                	<a href="http://www.liv.ac.uk/csd/security/passwords/"><img src="http://csdsite.liv.ac.uk/img/home-page/password-icon.svg" width="40" height="40" alt="Change Password"/></a>
                    <a href="http://www.liv.ac.uk/csd/security/passwords/">Change password</a>
                </li>
            	<li id="quick-links-email">
                	<a href="https://owa.liv.ac.uk/"><img src="http://csdsite.liv.ac.uk/img/home-page/email-icon.svg" width="40" height="40" alt="Webmail login"/></a>
                    <a href="https://owa.liv.ac.uk/">Webmail login</a>
                </li>
            	<li id="quick-links-apps-anywhere">
                	<a href="https://citrix.liv.ac.uk/Citrix/XenApp/auth/login.aspx"><img src="http://csdsite.liv.ac.uk/img/home-page/apps-anywhere-icon.svg"  width="40" height="40" alt="Apps Anywhere"/></a>
                    <a href="https://citrix.liv.ac.uk/Citrix/XenApp/auth/login.aspx">Apps Anywhere</a>
                </li>
            	<li id="quick-links-vital">
                	<a href="http://vital.liv.ac.uk/"><img src="http://csdsite.liv.ac.uk/img/home-page/vital-icon.svg" width="40" height="40" alt="VITAL login"/></a>
                    <a href="http://vital.liv.ac.uk/">VITAL login</a>
                </li>
            	<li id="quick-links-spider">
                	<a href="http://liverpool-life.liv.ac.uk"><img src="http://csdsite.liv.ac.uk/img/home-page/spider-icon.svg" width="40" height="40" alt="Liverpool Life"/></a>
                    <a href="http://liverpool-life.liv.ac.uk">Liverpool Life</a>
                </li>
            	<li id="quick-links-helpdesk">
                	<a href="http://www.liv.ac.uk/csd/helpdesk/"><img src="http://csdsite.liv.ac.uk/img/home-page/helpdesk-icon.svg" width="40" height="40" alt="Helpdesk"/></a>
                    <a href="http://www.liv.ac.uk/csd/helpdesk/">Helpdesk</a>
                </li>
                
            </ul>
        </nav>
        <!-- quick links ends -->
        
        <!-- hang line -->
        <hr class="hang-line" id="quick-links-hang-line">
        
        <!-- services begins -->
        <nav id="services" class="computing-home-module">
        	<h1>Our services</h1>
            <ul>
            	<li><a href="http://www.liv.ac.uk/csd/email/">Email</a></li>
            	<li><a href="http://www.liv.ac.uk/csd/printing/">Printing</a></li>
            	<li><a href="http://www.liv.ac.uk/csd/pccentres/">PC centres</a></li>
            	<li><a href="http://www.liv.ac.uk/csd/software/">Software</a></li>
            	<li><a href="http://www.liv.ac.uk/csd/my-files/">My files</a></li>
            	<li><a href="http://www.liv.ac.uk/csd/yourcomputer/">My computer</a></li>
            	<li><a href="http://www.liv.ac.uk/csd/mobiles-and-tablets/">Mobiles &amp; tablets</a></li>
			</ul>
            <a class="call-to-action" href="http://www.liv.ac.uk/csd/a-to-z/">A to Z of services</a>
        </nav>
        <!-- services ends -->

        <!-- hang line -->
        <hr class="hang-line" id="services-hang-line">
        
        <!-- support begins -->
        <nav id="support" class="computing-home-module">
        	<h1>More services</h1>
            <ul>
            	<li><a href="https://register.liv.ac.uk/register/">Activate your account</a></li>
            	<li><a href="http://www.liv.ac.uk/csd/quickstart/">IT Quickstart for students</a></li>
            	<li><a href="http://www.liv.ac.uk/csd/staff/">IT Fundamentals for staff</a></li>
            	<li><a href="http://www.liv.ac.uk/csd/biss/">Business systems</a></li>
            	<li><a href="http://www.liv.ac.uk/csd/training/">IT Training &amp; skills</a></li>
            	<li><a href="http://www.liv.ac.uk/csd/security/">Security</a></li>
            	<li><a href="http://www.liv.ac.uk/csd/regulations/">Regulations &amp; guidelines</a></li>
			</ul>
            <a class="call-to-action" href="http://www.liv.ac.uk/csd/helpdesk/">Contact the helpdesk</a>
        </nav>
        <!-- support ends -->

        <!-- hang line -->
        <hr class="hang-line" id="support-hang-line">
        
        <!-- news begins -->
        <div id="news" class="computing-home-module">

        	<h1>News</h1>
            <ul>
				<?php error_reporting(0);

                $conn = oci_connect($username, $password, $db_name);
                if (!$conn) {
                    $e = oci_error();
                    //trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
                }
                
                // Prepare the statement
                $stid = oci_parse($conn, "SELECT DISTINCT POSTER_ID, POSTER_END, POSTER_APPROVED, POSTER_TITLE, 
                REPLACE(REPLACE(REPLACE(REPLACE(POSTER_BODY,chr(13)||chr(10),'<br />'),'[URL]','<a href='),'[LABEL]','>'),'[END]','</a>')POSTER_BODY,
                SUBSTR(POSTER_BODY,1,INSTR(POSTER_BODY,'.')) POSTER_SUMMARY,
                TO_CHAR(POSTER_START,'DD Month YYYY') POSTER_FULLDATE,
                TO_DATE(POSTER_START) POSTER_START
                FROM RSS_POSTER
                where POSTER_CATEGORY_CSD='on' and POSTER_APPROVED='yes' and POSTER_ID in (select distinct CSD_LIST from RSS_CSD where CSD_TYPE='n')
                and poster_approved_by not in ('dclay', 'bucknell', 'q101')
                and SYSDATE >= POSTER_START
                and SYSDATE <= POSTER_END +1
                ORDER BY POSTER_START DESC
                ");
                if (!$stid) {
                    $e = oci_error($conn);
                    //trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
                
                }
                

                //clearTimeout();
                
                // Perform the logic of the query
                $r = oci_execute($stid);
                if (!$r) {
                    $e = oci_error($stid);
                    //trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
                }
                
                // Fetch the results of the query
                
                $rowcount = 0;
                while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                    //print "<tr>\n";
                    $thereisnews = true;
                    $colcount = 0;
                    foreach ($row as $item) {        
                        if ($rowcount <= 1){          
                          if ($colcount == 0){
                              $id = ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;");
                              }
                              elseif ($colcount == 3){
                              $title = ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;");
                              }
                          elseif ($colcount == 6){
                              $date = ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;");
                              }
                          elseif ($colcount == 5){
                              $body = ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;");              
                          }
                        $colcount++;
                        }
                    
                    }
                    if ($rowcount <= 1){ 
                    print '    <li><h2><a href="newsannouncements.php?posterid='. $id .'&amp;postertitle='. $title .'">' . html_entity_decode($title, ENT_QUOTES). '</a></h2>';
                    // print '    <li><h2><a href="newsannouncements.php?posterid='. $id .'&postertitle='. $title .'">' . html_entity_decode($title, ENT_QUOTES). ', '. $date .'</a></h2>';
                    print '    <p>' . html_entity_decode($body, ENT_QUOTES) . '';
                    print ' </p></li>';
                    }
                    $rowcount++;
                    }
                 
                
                oci_free_statement($stid);
                oci_close($conn);
                
                if ($thereisnews != TRUE){
                   print '    <div><h4>Study for the European Computing Driving Licence (ECDL)</h4>
                <p>Computing Services offers staff and students the opportunity to study and gain this IT qualification. We also offer the Advanced ECDL qualification in Word Processing. <a href="http://www.liv.ac.uk/csd/training/ecdl/index.htm">Find out more...</a></p>
                </div> '; 
                }
                
                ?>
            </ul>
            <a class="call-to-action" href="http://www.liv.ac.uk/csd/newsstories.php">View all news</a>
			<!-- 
        	<ul>
            	<li>
                	<h2><a href="#">A new way to check the current status of CSD services, 16 January 2012</a></h2>
                    <p>The Computing Services homepage has recently been updated to include a feature which displays the current service status.</p>
                </li>
            	<li>
                	<h2><a href="#">Working with email attachments - warning, 16 January 2012</a></h2>
                    <p>If you work on an open email attachment, make changes to it but do not actually save the document on your M: Drive or elsewhere, YOU WILL LOSE all of those changes once you close the document, regardless of how many times you 'Save' it.</p>
                </li>
            </ul>
            <a class="call-to-action" href="#">View all news</a>
            -->
            
        </div>
        <!-- news ends -->
        
        <!-- hang line -->
        <hr class="hang-line" id="news-hang-line">
        
        <!-- follow us begins -->
        <div id="follow-us" class="computing-home-module">
        	<h1>Follow us online</h1>
            <ul>
            	<li id="latest-tweet">
                	<!-- <img src="http://pcwww.liv.ac.uk/~haganp/responsive-demo/img/home-page/twitter-icon.png" alt="Twitter Icon" /> -->
                    <p><a title="Computing Services" href="https://twitter.com/#!/liverpoolcsd" data-user-id="76285419">@liverpoolcsd</a> - <span id="tweet">Follow liverpoolcsd on Twitter for updates and alerts in real time.</span></p>
                    <a href="https://twitter.com/#!/liverpoolcsd"><img src="http://csdsite.liv.ac.uk/img/twitter-bird.svg" alt="Twitter icon" /></a>
                    <a class="call-to-action" href="https://twitter.com/#!/liverpoolcsd">@liverpoolcsd on twitter</a>
                </li>
                <li id="csd-blog">
                    <p>Follow the blog for news, features, projects and our views on IT developments.</p>
                    <a href="http://www.liv.ac.uk/csd/blog/"><img src="http://csdsite.liv.ac.uk/img/home-page/blog-icon.svg" width="50" height="50" alt="Blog icon"/></a>
                    <br>
                    <a class="call-to-action" href="http://www.liv.ac.uk/csd/blog/">Read the latest blog post</a>
                </li>
            </ul>
        </div>
        <!-- follow us ends -->

        <!--  hang line -->
        <hr class="hang-line" id="follow-us-hang-line">
        
        <!-- mailing list begins -->
        <div id="mailing-list" class="computing-home-module">
        	<h1>Join our mailing list</h1>
            <p>Sign up for email alerts to receive our regular  updates and latest news.</p>
			<p>Email  <a href="mailto:listserv@liverpool.ac.uk?body=subscribe%20csdannounce%20FirstName%20LastName">listserv@liverpool.ac.uk</a> including the text <em>Subscribe csdannounce FirstName LastName</em>.</p>
            <img src="http://csdsite.liv.ac.uk/img/email-signup.jpg" alt="Email signup"/>
        </div>
        <!-- mailing list ends -->
        
        <!--  hang line -->
        <hr class="hang-line" id="mailing-list-hang-line">
        
        <!-- service-status begins -->
        <div id="service-status" class="computing-home-module">
        	<h1>Services status &amp; Announcements</h1>
			
			<?php
            //ini_set('display_errors', 1);
            //ini_set('log_errors', 1);
            //ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
            //error_reporting(E_ALL);      
            error_reporting(0);
            
            
                    
                    $url = 'http://status.liv.ac.uk/list.php';
            
                    //$url = 'http://status.liv.ac.uk/list.php';
                    //$url = 'http://localhost:81/FirstProject/list.php';
                    
                        $str = file_get_contents($url);
                        $strNoEnds = substr($str,221, -18);
                        $strSeperated = preg_split('</br>', $strNoEnds);
                        
                       
                        
                        $size = sizeof($strSeperated);        
                        For($i=1; $size, $i < $size; ++$i) {
                           $parts = explode(",", $strSeperated[$i]); 
                           $lastPart = sizeof($parts) - 1;
                          
                           ///GET STATUS
                           
                           ///AppsAnywhere
                          if (substr(@$parts[1], 0, 17)=='checkappsanywhere'){
                               $AppsAnywhereStatus = $parts[4];
                               ///reformat date/time  
            //                   $AppsAnywhereChecked = DateTime::createFromFormat('Y-m-d H:i:s', $parts[3]);
            //                   $AppsAnywhereChecked = $AppsAnywhereChecked->format('H:i d/m/Y')."\n";
                               $AppsAnywhereChecked = strtotime($parts[3]);
                               $AppsAnywhereChecked = date('H:i d/m/Y')."\n";
                               
                               if ($AppsAnywhereStatus == "1<"){
                                   $AppsAnywhereStatusAmber = TRUE;
                                   
                               }
                               elseif ($AppsAnywhereStatus == "2<"){
                                   $AppsAnywhereStatusRed = TRUE;
                                   
                               }
                               else {
                                   $AppsAnywhereStatusGreen = TRUE;
                                   
                               }
                           }
                           
                                          
                           ///Exchange
                           if (substr(@$parts[1], 0, 9)=='checkexch'){
                               $ExchangeStatus = $parts[4];
                               $ExchangeChecked = strtotime($parts[3]);
                               $ExchangeChecked = date('H:i d/m/Y')."\n";
                               
                               if ($ExchangeStatus == "1<"){
                                   $ExchangeStatusAmber = TRUE;
                                   
                                   
                               }
                               elseif ($ExchangeStatus == "2<"){
                                   //$ExchangeStatusRed = TRUE;
                                   switch(@$parts[1]){
                                       case "checkexch-imap":
                                           $ExchangeStatusRed1 = TRUE;
                                           break;
                                       case "checkexch-smtp1":
                                           $ExchangeStatusRed2 = TRUE;
                                           break;
                                       case "checkexch-smtp2":
                                           $ExchangeStatusRed3 = TRUE;
                                           break;
                                   }
                               }
                               else {
                                   //$ExchangeStatusGreen = TRUE;
                                   switch(@$parts[1]){
                                       case "checkexch-imap":
                                           $ExchangeStatusGreen1 = TRUE;
                                           break;
                                       case "checkexch-smtp1":
                                           $ExchangeStatusGreen2 = TRUE;
                                           break;
                                       case "checkexch-smtp2":
                                           $ExchangeStatusGreen3 = TRUE;
                                           break;
                                   }
                               }
                           }
                           
//                           ///EZProxy
//                           if (substr(@$parts[0], 1, 17)=='ezproxy.liv.ac.uk'){
//                               $EZproxyStatus = $parts[4];
//                               $EZproxyChecked = strtotime($parts[3]);
//                               $EZproxyChecked = date('H:i d/m/Y')."\n";                 
//                               
//                               if ($EZproxyStatus == "1<"){
//                                   $EZproxyStatusAmber = TRUE;
//                                   
//                               }
//                               elseif ($EZproxyStatus == "2<"){
//                                   //$EZproxyStatusRed = TRUE;
//                                   switch(@$parts[1]){
//                                       case "https check":
//                                           $EZproxyStatusRed1 = TRUE;
//                                           break;
//                                       case "http check":
//                                           $EZproxyStatusRed2 = TRUE;
//                                           break;                           
//                                   }
//                               }
//                               else {
//                                   //$EZproxyStatusGreen = TRUE;
//                                   switch(@$parts[1]){
//                                       case "https check":
//                                           $EZproxyStatusGreen1 = TRUE;
//                                           break;
//                                       case "http check":
//                                           $EZproxyStatusGreen2 = TRUE;
//                                           break;                           
//                                   }
//                               }
//                           }        


							///EZProxy
						   if (substr(@$parts[0], 1, 7)=='ezproxy'){
							   $EZproxyStatus = $parts[4];
							   $EZproxyChecked = strtotime($parts[3]);
							   $EZproxyChecked = date('H:i d/m/Y')."\n";                 
							   
							   if ($EZproxyStatus == "1<"){
								   $EZproxyStatusAmber = TRUE;
								   
							   }
							   elseif ($EZproxyStatus == "2<"){
								   //$EZproxyStatusRed = TRUE;
								   switch(@$parts[1]){
									   case "ezproxy-chk-lib":
										   $EZproxyStatusRed1 = TRUE;
										   break;
									   case "ezproxy-chk-news":
										   $EZproxyStatusRed2 = TRUE;
										   break;  
									   case "ezproxy-chk-liv":
										   $EZproxyStatusRed3 = TRUE;
										   break; 
								   }
							   }
							   else {
								   //$EZproxyStatusGreen = TRUE;
								   switch(@$parts[1]){
									   case "ezproxy-chk-lib":
										   $EZproxyStatusGreen1 = TRUE;
										   break;
									   case "ezproxy-chk-news":
										   $EZproxyStatusGreen2 = TRUE;
										   break; 
									   case "ezproxy-chk-liv":
										   $EZproxyStatusGreen3 = TRUE;
										   break; 
								   }
							   }
						   }        

                           ///Lusid
                          if (substr(@$parts[0], 1, 16)=='lusida.liv.ac.uk'){
                               $LusidaStatus = $parts[4];
                               $LusidaChecked = strtotime($parts[3]);
                               $LusidaChecked = date('H:i d/m/Y')."\n"; 
                               
                               if ($LusidaStatus == "1<"){
                                   $LusidaStatusAmber = TRUE;
                                   
                               }
                               elseif ($LusidaStatus == "2<"){
                                   //$LusidaStatusRed = TRUE;
                                   switch(@$parts[1]){
                                       case "http check":
                                           $LusidaStatusRed1 = TRUE;
                                           break;
                                       case "http form check":
                                           $LusidaStatusRed2 = TRUE;
                                           break;
                                       case "https check":
                                           $LusidaStatusRed3 = TRUE;
                                           break;
                                   }
                               }
                               else {
                                   //$LusidaStatusGreen = TRUE;
                                   switch(@$parts[1]){
                                       case "http check":
                                           $LusidaStatusGreen1 = TRUE;
                                           break;
                                       case "http form check":
                                           $LusidaStatusGreen2 = TRUE;
                                           break;
                                       case "https check":
                                           $LusidaStatusGreen3 = TRUE;
                                           break;
                                   }
                               }
                           }        
                           
                           ///Staff Home Drive
                           if (substr(@$parts[1], 0, 13)=='checkstaffmws'){
                               $StaffPCWWWStatus = $parts[4];
                               $StaffPCWWWChecked = strtotime($parts[3]);
                               $StaffPCWWWChecked = date('H:i d/m/Y')."\n";
                               
                               if ($StaffPCWWWStatus == "1<"){
                                   $StaffPCWWWStatusAmber = TRUE;
                                   
                               }
                               elseif ($StaffPCWWWStatus == "2<"){
                                   //$StaffPCWWWStatusRed = TRUE;
                                   switch(@$parts[1]){
                                       case "checkstaffmws-ufs1":
                                           $StaffPCWWWStatusRed1 = TRUE;
                                           break;
                                       case "checkstaffmws-ufs2":
                                           $StaffPCWWWStatusRed2 = TRUE;
                                           break;
                                       case "checkstaffmws-ufs3":
                                           $StaffPCWWWStatusRed3 = TRUE;
                                           break;
                                       case "checkstaffmws-ufs4":
                                           $StaffPCWWWStatusRed4 = TRUE;
                                           break;

                                   }
                               }
                               else {
                                   //$StaffPCWWWStatusGreen = TRUE;
                                   switch(@$parts[1]){
                                       case "checkstaffmws-ufs1":
                                           $StaffPCWWWStatusGreen1 = TRUE;
                                           break;
                                       case "checkstaffmws-ufs2":
                                           $StaffPCWWWStatusGreen2 = TRUE;
                                           break;
                                       case "checkstaffmws-ufs3":
                                           $StaffPCWWWStatusGreen3 = TRUE;
                                           break;
                                       case "checkstaffmws-ufs4":
                                           $StaffPCWWWStatusGreen4 = TRUE;
                                           break;

                                   }
                                   
                               }
                           }        
                           

                           
                           ///Webmail
                           if (substr(@$parts[1], 0, 8)=='checkowa'){
                               $WebmailStatus = $parts[4];
                               $WebmailChecked = strtotime($parts[3]);
                               $WebmailChecked = date('H:i d/m/Y')."\n";
                               
                               if ($WebmailStatus == "1<"){
                                   $WebmailStatusAmber = TRUE;
                                   
                               }
                               elseif ($WebmailStatus == "2<"){
                                   //$WebmailStatusRed = TRUE;
                                   switch(@$parts[1]){
                                       case "checkowa1":
                                           $WebmailStatusRed1 = TRUE;
                                           break;
                                       case "checkowa2":
                                           $WebmailStatusRed2 = TRUE;
                                           break;
                                       case "checkowa3":
                                           $WebmailStatusRed3 = TRUE;
                                           break;
                                   }
                                   
                               }
                               else {
                                   //$WebmailStatusGreen = TRUE;
                                   switch(@$parts[1]){
                                       case "checkowa1":
                                           $WebmailStatusGreen1 = TRUE;
                                           break;
                                       case "checkowa2":
                                           $WebmailStatusGreen2 = TRUE;
                                           break;
                                       case "checkowa3":
                                           $WebmailStatusGreen3 = TRUE;
                                           break;
                                   }
                               }
                           }        
                           
                           ///Student Digital University
                           if (substr(@$parts[0], 1, 10)=='portalstud'){
                               $PortalStudStatus = $parts[4];
                               $PortalStudChecked = strtotime($parts[3]);
                               $PortalStudChecked = date('H:i d/m/Y')."\n";
                               
                               if ($PortalStudStatus == "1<"){
                                   $PortalStudStatusAmber = TRUE;
                                   
                               }
                               elseif ($PortalStudStatus == "2<"){
                                   //$PortalStatusRed = TRUE;
                                   switch(@$parts[1]){
                                       case "portalstud-check":
                                           $PortalStudStatusRed1 = TRUE;
                                           break;
            //                           case "https form login":
            //                               $PortalStatusRed2 = TRUE;
            //                               break;
                                   }
                                   
                               }
                               else {
                                   //$PortalStatusGreen = TRUE;
                                   switch(@$parts[1]){
                                       case "portalstud-check":
                                           $PortalStudStatusGreen1 = TRUE;
                                           break;
            //                           case "https form login":
            //                               $PortalStatusGreen2 = TRUE;
            //                               break;
                                   }
                                   
                               }
                           }      
                           
                           ///Staff Digital University
                           if (substr(@$parts[0], 1, 11)=='portalstaff'){
                               $PortalStaffStatus = $parts[4];
                               $PortalStaffChecked = strtotime($parts[3]);
                               $PortalStaffChecked = date('H:i d/m/Y')."\n";
                               
                               if ($PortalStaffStatus == "1<"){
                                   $PortalStaffStatusAmber = TRUE;
                                   
                               }
                               elseif ($PortalStaffStatus == "2<"){
                                   //$PortalStatusRed = TRUE;
                                   switch(@$parts[1]){
                                       case "portalstaff-check":
                                           $PortalStaffStatusRed1 = TRUE;
                                           break;
            //                           case "https form login":
            //                               $PortalStatusRed2 = TRUE;
            //                               break;
                                   }
                                   
                               }
                               else {
                                   //$PortalStatusGreen = TRUE;
                                   switch(@$parts[1]){
                                       case "portalstaff-check":
                                           $PortalStaffStatusGreen1 = TRUE;
                                           break;
            //                           case "https form login":
            //                               $PortalStatusGreen2 = TRUE;
            //                               break;
                                   }
                                   
                               }
                           }
                           
                           ///Spider
                           if (substr(@$parts[1], 0, 11)=='checkspider'){
                               $SpiderStatus = $parts[4];
                               $SpiderChecked = strtotime($parts[3]);
                               $SpiderChecked = date('H:i d/m/Y')."\n";
                               
                               if ($SpiderStatus == "1<"){
                                   $SpiderStatusAmber = TRUE;
                                   
                               }
                               elseif ($SpiderStatus == "2<"){
                                   //$SpiderStatusRed = TRUE;
                                   switch(@$parts[1]){
                                       case "checkspider":
                                           $SpiderStatusRed1 = TRUE;
                                           break;
                                       case "checkspider2":
                                           $SpiderStatusRed2 = TRUE;
                                           break;
                                   }
                                   
                               }
                               else {
                                   //$SpiderStatusGreen = TRUE;
                                   switch(@$parts[1]){
                                       case "checkspider":
                                           $SpiderStatusGreen1 = TRUE;
                                           break;
                                       case "checkspider2":
                                           $SpiderStatusGreen2 = TRUE;
                                           break;
                                   }
                                   
                               }
                           }        
                           
                           ///Tulip
                            if (substr(@$parts[0], 1, 15)=='tulip.liv.ac.uk'){
                               $TulipStatus = $parts[4];
                               $TulipChecked = strtotime($parts[3]);
                               $TulipChecked = date('H:i d/m/Y')."\n"; 
                               
                               if ($TulipStatus == "1<"){
                                   $TulipStatusAmber = TRUE;
                                   
                               }
                               elseif ($TulipStatus == "2<"){
                                   //$TulipStatusRed = TRUE;
                                   switch(@$parts[1]){
                                       case "http check":
                                           $TulipStatusRed1 = TRUE;
                                           break;
                                       case "http form check":
                                           $TulipStatusRed2 = TRUE;
                                           break;
                                       case "https check":
                                           $TulipStatusRed3 = TRUE;
                                           break;
                                   }
                                   
                               }
                               else {
                                   //$TulipStatusGreen = TRUE;
                                   switch(@$parts[1]){
                                       case "http check":
                                           $TulipStatusGreen1 = TRUE;
                                           break;
                                       case "http form check":
                                           $TulipStatusGreen2 = TRUE;
                                           break;
                                       case "https check":
                                           $TulipStatusGreen3 = TRUE;
                                           break;
                                   }
                                   
                               }
                           }        
                           
                           ///Vital
                            if (substr(@$parts[0], 1, 5)=='vital'){
                               $VitalStatus = $parts[4];
                               $VitalChecked = strtotime($parts[3]);
                               $VitalChecked = date('H:i d/m/Y')."\n"; 
                               
                               if ($VitalStatus == "1<"){
                                   $VitalStatusAmber = TRUE;
                                   
                               }
                               elseif ($VitalStatus == "2<"){
                                   //$VitalStatusRed = TRUE;
                                   switch(@$parts[1]){
                                       case "https check":
                                           $VitalStatusRed1 = TRUE;
                                           break;
            //                           case "http form check":
            //                               $VitalStatusRed2 = TRUE;
            //                               break;
                                       
                                   }
                                   
                               }
                               else {
                                   //$VitalStatusGreen = TRUE;
                                   switch(@$parts[1]){
                                       case "https check":
                                           $VitalStatusGreen1 = TRUE;
                                           break;
            //                           case "http form check":
            //                               $VitalStatusGreen2 = TRUE;
            //                               break;
                                       
                                   }
                                   
                               }
                           }        
                           
                           ///Vocal
                           if (substr(@$parts[1], 0, 10)=='checkvocal'){
                               $VocalStatus = $parts[4];
                               $VocalChecked = strtotime($parts[3]);
                               $VocalChecked = date('H:i d/m/Y')."\n"; 
                               
                               if ($VocalStatus == "1<"){
                                   $VocalStatusAmber = TRUE;
                                   
                               }
                               elseif ($VocalStatus == "2<"){
                                   $VocalStatusRed = TRUE;
                                   
                               }
                               else {
                                   $VocalStatusGreen = TRUE;
                                   
                               }
                           }        
                           
                           ///Main University Website
                           if (substr(@$parts[0], 1, 14)=='wwwb.liv.ac.uk'){
                               $WebsiteStatus = $parts[4];
                               $WebsiteChecked = strtotime($parts[3]);
                               //$WebsiteChecked = gmdate('H:i')."\n"; 
							   $WebsiteChecked = substr($parts[3], 11, 5);
                               
                               if ($WebsiteStatus == "1<"){
                                   $WebsiteStatusAmber = TRUE;
                                   
                               }
                               elseif ($WebsiteStatus == "2<"){
                                   //$WebSiteStatusRed = TRUE;
                                   switch(@$parts[1]){
                                       case "http check":
                                           $WebsiteStatusRed1 = TRUE;
                                           break;
                                       case "https check":
                                           $WebsiteStatusRed2 = TRUE;
                                           break;
                                   }
                                   
                               }
                               else {
                                   //$WebSiteStatusGreen = TRUE;
                                   switch(@$parts[1]){
                                       case "http check":
                                           $WebsiteStatusGreen1 = TRUE;
                                           break;
                                       case "https check":
                                           $WebsiteStatusGreen2 = TRUE;
                                           break;
                                   }
                                                          
                               }
                           }       
                       
                          ///Registration
                           if (substr(@$parts[0], 1, 8)=='register'){
                               $PortalStatus = $parts[4];
                               $RegisterChecked = strtotime($parts[3]);
                               $RegisterChecked = date('H:i d/m/Y')."\n";
                               
                               if ($RegisterStatus == "1<"){
                                   $RegisterStatusAmber = TRUE;
                                   
                               }
                               elseif ($RegisterStatus == "2<"){
                                   //$RegisterStatusRed = TRUE;
                                   switch(@$parts[1]){
                                       case "http form":
                                           $RegisterStatusRed1 = TRUE;
                                           break;
                                       case "Oracle check":
                                           $RegisterStatusRed2 = TRUE;
                                           break;
                                   }
                                   
                               }
                               else {
                                   //$RegisterStatusGreen = TRUE;
                                   switch(@$parts[1]){
                                       case "http form":
                                           $RegisterStatusGreen1 = TRUE;
                                           break;
                                       case "Oracle check":
                                           $RegisterStatusGreen2 = TRUE;
                                           break;
                                   }
                                   
                               }
                           }        
                        }
                        
                        ///echo '<table id="heading" width=330px style="border-spacing:0px"><tr style="height:20px;font-size:12px;line-height:20px;background-color:#A5927B;color:white;font-family:verdana;font-weight:bold;"><td>CSD Service Status</td></tr></table>';
                        ///echo '<table id="services" width=330px style="border-spacing:0px;padding:5px"><tr style="height:5px"><td></td></tr>';
                        echo '<ul id="status-list">';
                        ///SHOW RESULTS
                        
                                   
                        ///Outlook
                        if (isset ($ExchangeStatusGreen1, $ExchangeStatusGreen2, $ExchangeStatusGreen3)) {
                            echo '<li class="outlook green">Outlook is running as expected</li>';
                             }   
                         elseif (isset ($ExchangeStatusRed1, $ExchangeStatusRed2, $ExchangeStatusRed3)) {
                            echo '<li class="outlook red">Outlook is currently unavailable</li>';
                             }
                         else {
                            echo '<li class="outlook amber">Outlook is at risk</li>';
                         }
                         
                         ///Webmail
                         if (isset ($WebmailStatusGreen1, $WebmailStatusGreen2, $WebmailStatusGreen3)) {
                            echo '<li class="webmail green">Webmail is running as expected</li>';
                             }  
                         elseif (isset ($WebmailStatusRed1, $WebmailStatusRed2, $WebmailStatusRed3)) {
                            echo '<li class="webmail red">Webmail is currently unavailable</li>';
                             }
                         else {                   
                            echo '<li class="webmail amber">Webmail is at risk</li>';
                             }
                             
                             ///M: drive
                         if (isset ($StaffPCWWWStatusGreen1, $StaffPCWWWStatusGreen2, $StaffPCWWWStatusGreen3, $StaffPCWWWStatusGreen4)) {
                         	 echo '<li class="pcwww green">M: drive is running as expected</li>';
                             }  
                         elseif (isset ($StaffPCWWWStatusRed1, $StaffPCWWWStatusRed2, $StaffPCWWWStatusRed3, $StaffPCWWWStatusRed4)) {
                         	 echo '<li class="pcwww red">M: drive is currently unavailable</li>';
                             }
                         else {
                         	 echo '<li class="pcwww amber">M: drive is at risk</li>';
                             }

                             
                              ///Vital
                         if (isset ($VitalStatusGreen1)) {
                            echo '<li class="vital green">VITAL is running as expected</li>';
                             }  
                         elseif (isset ($VitalStatusRed1)) {
                            echo '<li class="vital red">VITAL is currently unavailable</li>';
                             }
                         else {
                            echo '<li class="vital amber">VITAL is at risk</li>';
                             }
                             
                              ///Spider
                         if (isset ($SpiderStatusGreen1, $SpiderStatusGreen2)) {
                            echo '<li class="spider green">Liverpool Life is running as expected</li>';
                             }  
                         elseif (isset ($SpiderStatusRed1, $SpiderStatusRed2)) {
                            echo '<li class="spider red">Liverpool Life is currently unavailable</li>';
                             }
                         else {                 
                            echo '<li class="spider amber">Liverpool Life is at risk</li>';
                             }
                             
                             ///Tulip
                         if (isset ($TulipStatusGreen1, $TulipStatusGreen2, $TulipStatusGreen3)) {
                            echo '<li class="tulip green">TULIP is running as expected</li>';
                             }  
                         elseif (isset ($TulipStatusRed1, $TulipStatusRed2, $TulipStatusRed3)) {
                            echo '<li class="tulip red">TULIP is currently unavailable</li>';
                             }
                         else {                 
                            echo '<li class="tulip amber">TULIP is at risk</li>';
                             }
                             
                             ///Apps Anywhere
                        if (isset ($AppsAnywhereStatusGreen)) {
                            echo '<li class="apps-anywhere green">Apps Anywhere is running as expected</li>';
                            }  
                         elseif (isset ($AppsAnywhereStatusRed)) {
                            echo '<li class="apps-anywhere red">Apps Anywhere is currently unavailable</li>';
                            }
                         else {
                            echo '<li class="apps-anywhere amber">Apps Anywhere is at risk</li>';
                            }
                            
                             ///Vocal
                         if (isset ($VocalStatusGreen)) {
                            echo '<li class="vocal green">VOCAL is running as expected</li>';
                             }  
                         elseif (isset ($VocalStatusRed)) {
                            echo '<li class="vocal red">VOCAL is currently unavailable</li>';
                             }
                         else {                 
                            echo '<li class="vocal amber">VOCAL is at risk</li>';
                             }
                             
                              ///Lusid
                         if (isset ($LusidaStatusGreen1, $LusidaStatusGreen2, $LusidaStatusGreen3)) {
                            echo '<li class="lusid green">LUSID is running as expected</li>';
                             }  
                         elseif (isset ($LusidaStatusRed1, $LusidaStatusRed2, $LusidaStatusRed3)) {
                            echo '<li class="lusid red">LUSID is currently unavailable</li>';
                             }
                         else {
                            echo '<li class="lusid amber">LUSID is at risk</li>';
                             }
                         
                         ///Online journal access
                         if (isset ($EZproxyStatusGreen1, $EZproxyStatusGreen2)) {
                            echo '<li class="journals green">Online journals are running as expected</li>';
                               }  
                         elseif (isset ($EZproxyStatusRed1, $EZproxyStatusRed2)) {
                            echo '<li class="journals red">Online journals are currently unavailable</li>';
                             }
                         else {
                            echo '<li class="journals amber">Online journals are at risk</li>';
                             }
                             
                             ///Register
                             
                         if (isset ($RegisterStatusGreen1, $RegisterStatusGreen2)) {
                            echo '<li class="registration green">Account activations are running as expected</li>';
                             }  
                         elseif (isset ($RegisterStatusRed1, $RegisterStatusRed2)) {
                            echo '<li class="registration red">Account activations are currently unavailable</li>';
                             }
                         else {                 
                            echo '<li class="registration amber">Account activations are at risk</li>';
                             }
                                          
                          ///Student Digital University              
                         if (isset ($PortalStudStatusGreen1)) {
                            echo '<li class="studu green">Student Digital University is running as expected</li>';
                             }  
                         elseif (isset ($PortalStudStatusRed1)) {
                            echo '<li class="studu red">Student Digital University is currently unavailable</li>';
                             }
                         else {                 
                            echo '<li class="studu amber">Student Digital University is at risk</li>';
                             }
                             
                             ///Staff Digital University              
                         if (isset ($PortalStaffStatusGreen1)) {
                            echo '<li class="stadu green">Staff Digital University is running as expected</li>';
                             }  
                         elseif (isset ($PortalStaffStatusRed1)) {
                            echo '<li class="stadu red">Staff Digital University is currently unavailable</li>';
                             }
                         else {                 
                            echo '<li class="stadu amber">Staff Digital University is at risk</li>';
                             }
                         
                             
                         
                             echo '<li>Last checked today at '.$WebsiteChecked.'</li>';
                             echo '</ul>';
                    ?>


            <hr>
 
			<ul>

			<?php
            
            
            //        $db = "(DESCRIPTION =
            //    (ADDRESS_LIST =
            //      (ADDRESS = (PROTOCOL = TCP)(HOST = announce.liv.ac.uk)(PORT = 1521))
            //    )
            //    (CONNECT_DATA =
            //      (SERVICE_NAME = announce)
            //    )
            //  )";
                    
            $conn = oci_connect($username, $password, $db_name);
            if (!$conn) {
                $e = oci_error();
                //trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
            }
            
            // Prepare the statement
            $stid = oci_parse($conn, "SELECT DISTINCT POSTER_ID, POSTER_END, POSTER_APPROVED, POSTER_TITLE, 
            REPLACE(REPLACE(REPLACE(REPLACE(POSTER_BODY,chr(13)||chr(10),'<br />'),'[URL]','<a href='),'[LABEL]','>'),'[END]','</a>')POSTER_BODY,
            SUBSTR(POSTER_BODY,1,INSTR(POSTER_BODY,'.')) POSTER_SUMMARY,
            TO_CHAR(POSTER_START,'DD Month YYYY') POSTER_FULLDATE,
            TO_DATE(POSTER_START) POSTER_START
            FROM RSS_POSTER
            where POSTER_CATEGORY_CSD='on' and POSTER_APPROVED='yes' and POSTER_ID in (select distinct CSD_LIST from RSS_CSD where CSD_TYPE='s') and POSTER_APPROVED_BY not in ('dclay', 'bucknell', 'q101')
            and SYSDATE >= POSTER_START
            and SYSDATE <= POSTER_END +1
            ORDER BY POSTER_START DESC
            ");
            if (!$stid) {
                $e = oci_error($conn);
                //trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
            }
            
            // Perform the logic of the query
            $r = oci_execute($stid);
            if (!$r) {
                $e = oci_error($stid);
                //trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
            }
            
            // Fetch the results of the query
            
            $rowcount = 0;
            while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                //print "<tr>\n";
                $thereisannounce = true;
                $colcount = 0;
                foreach ($row as $item) {        
                    if ($rowcount <= 4){          
                      if ($colcount == 0){
                          $id = ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;");
                          }
                          elseif ($colcount == 3){
                          $title = ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;");
                          }
                      elseif ($colcount == 6){
                          $date = ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;");
                          }
                      elseif ($colcount == 5){
                          $body = ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;");              
                      }
                    $colcount++;
                    }
                
                }
                if ($rowcount <= 2){ 
                print '    <li><h2><a href="newsannouncements.php?posterid='. $id .'&postertitle='. $title .'">' . html_entity_decode($title, ENT_QUOTES) . '</a></h2></li>';
                }
                $rowcount++;
                }
             
            
            oci_free_statement($stid);
            oci_close($conn);
            
            if ($thereisannounce != TRUE){
               print '    <div><p>No current service announcements </p>
            <p>Reminder: All users of University IT facilities must ensure they comply with the Regulations. The <a href="http://www.liv.ac.uk/csd/regulations/index.htm">full Regulations</a>, as well as a <a href="http://www.liv.ac.uk/csd/regulations/regulationssummary.pdf">summary</a> are available, alongside <a href="http://www.liv.ac.uk/csd/regulations/index.htm">related policies and 
            <span lang="en-gb">codes of practice</span></a>.</p>
            </div>'; 
            }
            
            ?>
            
            </ul>
            <a class="call-to-action" href="http://www.liv.ac.uk/csd/newsstories.php">View service announcements</a>
        </div>
        
                
    </section>       

	<hr>
    
    <footer role="contentinfo">
        <div id="address">
            <p class="vcard">
                <span class="fn org">Computing Services, University of Liverpool</span><br>
                <span class="adr">
                <span class="street-address">Brownlow Hill</span>, <span class="locality">Liverpool</span> <span class="postal-code">L69 3GG</span>, <span class="country-name">United Kingdom</span><br>
                </span>
                <span class="tel">+44 (0)151 794 2000</span>
            </p>
        </div>
        <div id="global-footer">
            <p class="float-left">&copy; University of Liverpool - a member of The Russell Group</p>
            <p class="floar-right"><a href="http://www.liv.ac.uk/contacts/">Contacts</a> | <a href="http://www.liv.ac.uk/maps/">Map</a> | <a href="http://www.liv.ac.uk/legal/">Legal</a> | <a href="http://www.liv.ac.uk/accessibility/">Accessibility</a></p>
        </div>
    </footer>
    
</div> <!--! end of #container -->

<!-- feedback popup 
<div id="feedback-popup">
	<h1>Give us your feedback</h1>
    <p>We'd love to hear what you think about our new home&nbsp;page before it goes live - email us at <a href="mailto:helpdesk@liverpool.ac.uk">helpdesk@liverpool.ac.uk</a>.</p>
	<img width="29" height="29" src="http://pcwww.liv.ac.uk/~haganp/responsive-demo/close-button.png" alt="close">
</div>
-->

<!-- JavaScript at the bottom for fast page loading -->

<!-- Grab Google CDN's jQuery. fall back to local if necessary -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="//www.liv.ac.uk/scripts/jquery-1.4.2.js"><\/script>')</script>

<!-- selectivizr which enables advanced css selectors in older browsers -->
<!--[if lt IE 9 ]>
    <script src="http://csdsite.liv.ac.uk/js/libs/selectivizr-min.js"></script>
<![endif]-->

<!-- scripts concatenated and minified via ant build script-->
<script src="http://csdsite.liv.ac.uk/js/csd-plugins.js"></script>
<!-- end concatenated and minified scripts-->


<!-- Change UA-XXXXX-X to be your site's ID -->
<script>
window._gaq = [['_setAccount','UA-2638755-3'],['_trackPageview'],['_trackPageLoadTime']];
Modernizr.load({
  load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
});
</script>


<!-- crazy egg -->
<!-- crazy egg -->

<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName('script')[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0012/0987.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>


<!-- load tweets... 
<script src="http://twitterjs.googlecode.com/svn/trunk/src/twitter.min.js" type="text/javascript"></script>
<script type="text/javascript">
getTwitters('tweet', { 
  id: 'liverpoolcsd', 
  count: 1, 
  enableLinks: true, 
  ignoreReplies: true, 
  clearContents: true,
  template: '"%text%"'
});
</script>-->
<!-- end tweets -->


<!-- feedback popup 
<script>

// slide banner down and set local storage to remember that the banner is not hidden anymore
function popupFade(){
	setTimeout(function() {
		$('#feedback-popup').fadeIn(1500);
		localStorage.setItem('hidePopup', 'no');
	},2000);
}

$(document).ready(function(){
	
	if ($(window).width() >= 600){
	
		//if the is no record of the banner either being shown or hidden then slide it on down
		if( (localStorage.getItem("hidePopup") !== 'yes')){
			popupFade();
		}
				
		//if someone clicks the close button then get rid, and rememer that they got rid
		$('#feedback-popup img').live('click', function(event){
			event.preventDefault();
			$('#feedback-popup').fadeOut('fast');
			localStorage.setItem('hidePopup', 'yes');
		});
	
	}

});
</script>-->
  
  
</body>
</html>












 