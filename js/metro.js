// JavaScript Document

$(document).ready(function(){

	
	/* ----- home page slider ----- */
	/* ----- home page slider ----- */
	
	/* set the height of the cotinaer to accomodate the largest copy */
	var arr = new Array(2);
	arr[0] = $('.slide:eq(0)').height();
	arr[1] = $('.slide:eq(1)').height();
	arr[2] = $('.slide:eq(2)').height();
	var largest = arr.sort(function(a,b){return a - b}).slice(-1);
	largest = parseInt(largest,10);
	console.log(arr);
	$('#slider').css('min-height', (largest + 40));
	
	
	/* count the number of slider divs */
	var slideNumber = ($('.slide').size() -1);
	/* store whether the mouse is over the slider - used to pause the animations */
	var hoverState = false;
	/* counter */
	var i=0;


	/* the actual animation */
	function animateSlide(){
		$('#slider .active').animate({
			left: '-100%'
			}, 2000, function() {
				$('#slider .active').removeClass('active').css({left:'100%'});
		});	
		$('#slider .next').animate({
			left: '0%'
			}, 2000, function() {
			$('#slider .next').removeClass('next');
		});	
	}


	/* set the classes needed for the animation, then call it */
	function assignClasses(){
		if(hoverState == false){	
			if(i<slideNumber){
				$('.slide:eq(' + i + ')').addClass('active');
				$('.slide:eq(' + (i+1) + ')').addClass('next');
				animateSlide();
				i++;
			}else if (i==slideNumber){
				$('.slide:eq(' + i + ')').addClass('active');
				$('.slide:eq(0)').addClass('next');
				animateSlide();
				i=0;
			}
		}
	}


	/* trigger the animation every 5 seconds, except on small-screen devices */
	if(screen.width > 550){
		setInterval(assignClasses,7000);
	}
	

	/* watch for people hovering over the slider */
	$("#slider").mouseenter(function(){	
		hoverState = true;
		console.log(hoverState);
	});
	$("#slider").mouseleave(function(){	
		hoverState = false;
		console.log(hoverState);
	});


	/* ---- resizing videos ----- */
	/* ---- resizing videos ----- */
	
	$(document).ready(function(){
		
		var playerWidth = $('#body_frameplayer').width();
		var	playerHeight = ((playerWidth / 100) * 57) + (30);
	
		$('#body_frameplayer iframe').css({'width' : playerWidth, 'height' : playerHeight});
		
		$(window).resize(function() {
			playerWidth = $('#body_frameplayer').width();
			playerHeight = ((playerWidth / 100) * 57) + (30);
			$('#body_frameplayer iframe').css({'width' : playerWidth, 'height' : playerHeight});
		});
			
		// if the browser is IE8, force a quick and dirty refresh of the iFrame src, to make it re-size properly.
		
		function ieResize(){
			var iframeSrc = $('iframe').attr('src');
			$('iframe').attr('src', iframeSrc);
		}
		
		$(function(){
		   if ($('html').hasClass('ie8')) {
			   //alert('Re-sizing for IE8');
			   setTimeout(ieResize,10);
		   }
		});
		
	});


	/* ---- mobile navigation ----- */
	/* ---- mobile navigation ----- */

	function changeLocation(menuObj) {
		var i = menuObj.selectedIndex;
		if(i > 0) {
		window.location = menuObj.options[i].value;
		}
	}
	
	var jumpUrl = new Array();
	var jumpText = new Array();
	var jumpCount = 0;
		
	//create and insert an option menu with a couple of ready links
	$("<select onChange='javascript:changeLocation(this)'>").appendTo(".horizontal-nav");
	$("<option class='selector' value=''>Navigation</option>").appendTo(".horizontal-nav select");

	//grab the href and text from each link in the hidden 'all-children' navigation and chuck them in an array
	$('.horizontal-nav a').each(function(){
		jumpUrl.push($(this).attr('href'));
		jumpText.push($(this).text());
	});

	//get rid of the all-children markup
	//$('.horizontal-nav ul').remove();

	//count the number of values in each array, then generate the options for the jump menu from the arrays
	while (jumpCount<(jumpText.length))
		{
			$("<option class='selector' value='" + jumpUrl[jumpCount] + "' >" + jumpText[jumpCount] + '</option>').appendTo('.horizontal-nav select');
			jumpCount++;
		}

	//stick a home link into the jump menu	
	$("<option class='selector' value='http://www.liverpool.ac.uk/csd/'>Computing Services Home</option>").appendTo(".horizontal-nav select");
	
	//finally, add the ready class so that the menu never displays until the menu has finsihed being built
	$('#site-navigation select').addClass('ready');


});
