// JavaScript Document

var url = window.location.pathname;
var previewUrl;
var sectionName;

//console.log(sessionStorage.getItem('hideBanner'));

// figure out whether we're in one of the parts of the site that is changing, and store it in a variable
if (url.indexOf("/csd/printing/") >= 0){
	sectionName ="Printing";
	previewUrl ="http://cms.liv.ac.uk/csd/printing";	
} else if (url.indexOf("/csd/email/") >= 0){
	sectionName ="Email";
	previewUrl ="http://cms.liv.ac.uk/csd/email/";	
} else if (url.indexOf("/csd/security/") >= 0){
	sectionName ="Security";
	previewUrl ="http://cms.liv.ac.uk/csd/security/";	
} else {
	sectionName ="offSite";
}
//console.log(sectionName);


// slide banner down and set local storage to remember that the banner is not hidden anymore
function bannerSlide(){
	setTimeout(function() {
		$('<div id="alert-banner"><h1><strong>This page is changing!</strong> On campus? Preview the <a href="' + previewUrl + '">new ' + sectionName + ' pages</a>, coming soon...</h1><a href="#" id="close-button"><img src="http://pcwww.liv.ac.uk/~haganp/responsive-demo/close-button.png" width="29" height="29" /></a></div>').insertBefore('#header');		
		$('#alert-banner').slideDown(1500);
		sessionStorage.setItem('hideBanner', 'no');
	},2000);
}

$(document).ready(function(){
	
	//if the is no record of the banner either being shown or hidden then slide it on down
	if( (sessionStorage.getItem("hideBanner") === null) && (sectionName == "Printing" || sectionName == "Email" || sectionName == "Security") ){
		bannerSlide();
	}
	
	if( (sessionStorage.getItem("hideBanner") == "no") && (sectionName == "Printing" || sectionName == "Email" || sectionName == "Security") ){
		$('<div id="alert-banner" class="persist"><h1><strong>This page is changing!</strong> On campus? Preview the <a href="' + previewUrl + '">new ' + sectionName + ' pages</a>, coming soon...</h1><a href="#" id="close-button"><img src="http://pcwww.liv.ac.uk/~haganp/responsive-demo/close-button.png" width="29" height="29" /></a></div>').insertBefore('#header');		
	}
		
	//if someone clicks the close button then get rid, and rememer that they got rid
	$('#close-button').live('click', function(event){
		event.preventDefault();
		$('#alert-banner').slideUp('fast');
		sessionStorage.setItem('hideBanner', 'yes');
	});

});
