// usage: log('inside coolFunc', this, arguments);
// paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function(){
  log.history = log.history || [];   // store logs to an array for reference
  log.history.push(arguments);
  if(this.console) {
    arguments.callee = arguments.callee.caller;
    var newarr = [].slice.call(arguments);
    (typeof console.log === 'object' ? log.apply.call(console.log, console, newarr) : console.log.apply(console, newarr));
  }
};

// make it safe to use console.log always
(function(b){function c(){}for(var d="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,timeStamp,profile,profileEnd,time,timeEnd,trace,warn".split(","),a;a=d.pop();){b[a]=b[a]||c}})((function(){try
{console.log();return window.console;}catch(err){return window.console={};}})());




/* Author: Paul Hagan, University of Livrpool */

// ----- test for svg support, and swap out banner images for png if not available -----//
// ----- test for svg support, and swap out banner images for png if not available -----//

$(document).ready(function(){
	if (!Modernizr.svg){
			$('img').each(function(){
			var imgSrc = $(this).attr("src");
			if (imgSrc.indexOf('svg') >= 0){
				imgSrc = imgSrc.split("svg")
				$(this).attr("src", imgSrc[0] + "png")
			}
		});
	}
});


// ----- javascript to make the jump menu work ----- //
// ----- javascript to make the jump menu work ----- //
	
// i'm not mad keen on this, and should probably re-write it...
function changeLocation(menuObj) {
	var i = menuObj.selectedIndex;
	if(i > 0) {
	window.location = menuObj.options[i].value;
	}
}


// ----- everything else ----- //
// ----- everything else ----- //

$(document).ready(function() {
	


	// ----- mobile jump menu ----- //
	// ----- mobile jump menu ----- //
	
	var jumpUrl = new Array();
	var jumpText = new Array();
	var jumpCount = 0;
	
	//create and insert an option menu with a couple of ready links
	$("<select onChange='javascript:changeLocation(this)'>").appendTo("#site-navigation");
	$("<option class='selector' value=''>Navigation</option>").appendTo("#site-navigation select");

	//grab the href and text from each link in the hidden 'all-children' navigation and chuck them in an array
	$('#all-children a').each(function(){
		jumpUrl.push($(this).attr('href'));
		jumpText.push($(this).text());
	});

	//get rid of the all-children markup
	$('#all-children').remove();

	//count the number of values in each array, then generate the options for the jump menu from the arrays
	while (jumpCount<(jumpText.length))
		{
			$("<option class='selector' value='" + jumpUrl[jumpCount] + "' >" + jumpText[jumpCount] + '</option>').appendTo('#site-navigation select');
			jumpCount++;
		}

	//stick a home link into the jump menu	
	if($('#computing-services')){
		$("<option class='selector' value='#'>Computing Services Home</option>").appendTo("#site-navigation select");
	}
	
	//finally, add the ready class so that the menu never displays until the menu has finsihed being built
	$('#site-navigation select').addClass('ready');



	// ----- classes for the staus list on the CSD home page ----- //
	// ----- classes for the staus list on the CSD home page ----- //

	if ($('.red').length != 0) {
		$('#status-list').addClass("service-alert");
	} else if ($('.amber').length != 0) {
		$('#status-list').addClass("service-alert");
	} else {
		$('#status-list').addClass("service-ok");
		$('<li>All services are running normally.</li>').prependTo('#status-list');
	}


	
	/* ----- for small screen devices that have not chosen the desktop view re-order the page a litte ------ */
	/* ----- for small screen devices that have not chosen the desktop view re-order the page a litte ------ */
	
	if ((screen.width < 700) && (localStorage.getItem('screenKey') !=="non-responsive")) {
		$("#promo-spot, #promo-spot-hang-line").insertBefore("#services");
		$(".microsite-home .sidebar").insertBefore(".content");
		$("#search input[type=submit]").attr("value", "Go!");
		$("#breadcrumb").insertAfter("#masthead");
	}
	
	$("#search input[type=submit]").addClass("ready");


	/* ----- check if people actually want the responsive version ------ */
	/* ----- check if people actually want the responsive version ------ */
	
	// declare the variables for local storage
	var screenKey;
	var screenValue;
	
	// if the screen width is less that 700px, and there is no preference already stored...
	if ((screen.width < 700) && (localStorage.getItem('screenKey') === "null")){
			
		// insert the code to choose desktop or mobile picker, then slide it down...
		$("<div id='chooser'><p>Hi there, you look like a smartphone user. Which site would you prefer?</p><ul class='clearfix'><li><a href='#' class='call-to-action'>Desktop</a></li><li><a href='#' class='call-to-action float-right'>Mobile</a></li></ul><p>(you can always change your mind)</p></div>").insertBefore("#masthead");	
		$("#chooser").slideDown("slow");
			
		// if someone asks for the desktop version			
		$("#chooser a:first").click(function(){
			screenValue = "non-responsive";
			localStorage.setItem('screenKey', screenValue);
			$("#chooser").slideUp("slow", function(){
				$("body").removeClass("responsive");
				$("#promo-spot, #promo-spot-hang-line").insertBefore("#quick-links");
				$(".microsite-home .sidebar").insertAfter(".content");
				$('meta[name=viewport]').attr('content', 'width=1001,initial-scale=0.5');
			});
		});
		
		// if someone asks for the mobile version
		$("#chooser a:last").click(function(){
			screenValue = "responsive";
			localStorage.setItem('screenKey', screenValue);
			$("#chooser").slideUp("slow", function(){
				$("body").addClass("responsive");
				$('meta[name=viewport]').attr('content', 'width=device-width,initial-scale=1');
			});
		});
	
	}
	
	// if someone has chosen desktop version previously give it to them
	if((screen.width < 700) && (localStorage.getItem('screenKey') == "non-responsive" )){
		$("body").removeClass("responsive");
		$('meta[name=viewport]').attr('content', 'width=1001,initial-scale=0.5');
		$("<a id='reset' href='#' class='call-to-action large-desktop-link'>Visit mobile site</a><hr>").insertAfter("footer");
		$("#reset").click(function(){
			screenValue = "responsive";
			localStorage.setItem('screenKey', screenValue);
			location.reload(true);
		});
	}
	
	else if ((screen.width < 700) && (localStorage.getItem('screenKey') !== "non-responsive" )){
		$("<a id='reset' href='#' class='call-to-action'>Visit desktop site</a><hr>").insertAfter("footer");
		$("#reset").click(function(){
			screenValue = "non-responsive";
			localStorage.setItem('screenKey', screenValue);
			location.reload(true);
		});
	}
	
	/*
	// if someone has chosen desktop version previously give it to them
	if((screen.width < 700) && (localStorage.getItem('screenKey') == "non-responsive" )){
		$("body").removeClass("responsive");
		$('meta[name=viewport]').attr('content', 'width=1001,initial-scale=0.5');
	}
	
	// insert a reset link, and watch for clicks...
	if (screen.width < 700){
		$("<a id='reset' href='#' class='call-to-action'>Visit desktop site</a><hr>").insertAfter("footer");
		$("#reset").click(function(){
			screenValue = "null";
			localStorage.setItem('screenKey', screenValue);
			location.reload(true);
		});
	}
	*/



	/* ----- strip out inline CSS values for width and height on images ------ */
	/* ----- strip out inline CSS values for width and height on images ------ */
	
	$('#main-content img').attr('style','');


	/* ----- search selector, desktop only ----- */
	/* ----- search selector, desktop only ----- */

	/* on page load default the searh to computing services */
	if (screen.width > 700){
		$('#search').attr('action','http://google.liv.ac.uk/search');
		$('#search input[name=site]').attr('value','CSD');
		$('#search input[name=proxystylesheet]').attr('value','default_frontend');
	}

	// generate the search select div and radio buttons
	$('<div class="search-select"><span>Search within:</span><br><label><input name="search-select" type="radio" id="search-select_0" value="radio" checked ><span>Computing Services</span></label><br><label><input type="radio" name="search-select" value="radio" id="search-select_1"><span>University of Liverpool website</span></label></div>').appendTo('#search');
	$('.search-input').focus(function() {
		if (screen.width > 700){
			$('div.search-select').slideDown('medium');
			$(document).bind('focusin.search-select click.search-select',function(e) {
				if ($(e.target).closest('.search-select, .search-input').length) return;
				$(document).unbind('.search-select');
				$('div.search-select').slideUp('medium');
			});
		}
	});
	$('div.search-select').hide();
	
	//when a user selects either CSD or the whole site update the search form properties
	$('.search-select #search-select_0').click(function(){
		$('#search').attr('action','http://google.liv.ac.uk/search');
		$('#search input[name=site]').attr('value','CSD');
		$('#search input[name=proxystylesheet]').attr('value','default_frontend');
	});
	$('.search-select #search-select_1').click(function(){
		$('#search').attr('action','http://www.liv.ac.uk/search')
		$('#search input[name=site]').attr('value','default_collection');
		$('#search input[name=proxystylesheet]').attr('value','default09');
	});

	


});


// ----- cruft... ----- //
// ----- cruft... ----- //


$(document).ready(function() {
	//$('#breadcrumb a[href=/computing-services/index.php]').attr('href','http://www.liv.ac.uk/csd');
	$('#breadcrumb a:eq(1)').attr('href','http://www.liv.ac.uk/csd');
});

// ----- generate a jump menu and insert it after the main navigation ----- // --- 
// ----- generate a jump menu and insert it after the main navigation ----- //

//this is version 1 that createdd a jump menu that doesn't include all the links from the existing left hand nav, as it just re-wrote the existing nav ul

/*

var linkCount; 
var linkText;
var linkHref;


// count the number of links in the left hand nav
linkCount = $('#navigation li').length;

// append a select element to the main navigation
$("<select onChange='javascript:changeLocation(this)'>").appendTo("#site-navigation");
$("<option class='selector' value=''>Navigate to...</option>, <option class='selector' value='http://cms.liv.ac.uk/computing-services/'>CSD Home</option>").appendTo("#site-navigation select");

// for each link grab the text & href, then create an option element, and append it to the select created above
$("#site-navigation li a").each(function(){
	linkText = $(this).text();
	linkHref = $(this).attr('href');
	$("<option class='selector' value='" + linkHref + "' >" + linkText + "</option>").appendTo("#site-navigation select");
});

*/


