// usage: log('inside coolFunc', this, arguments);
// paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function(){
  log.history = log.history || [];   // store logs to an array for reference
  log.history.push(arguments);
  if(this.console) {
    arguments.callee = arguments.callee.caller;
    var newarr = [].slice.call(arguments);
    (typeof console.log === 'object' ? log.apply.call(console.log, console, newarr) : console.log.apply(console, newarr));
  }
};

// make it safe to use console.log always
(function(b){function c(){}for(var d="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,timeStamp,profile,profileEnd,time,timeEnd,trace,warn".split(","),a;a=d.pop();){b[a]=b[a]||c}})((function(){try
{console.log();return window.console;}catch(err){return window.console={};}})());


/* Author: Paul Hagan, University of Livrpool */

// ----- test for svg support, and swap out banner images for png if not available -----//
// ----- test for svg support, and swap out banner images for png if not available -----//

$(document).ready(function(){
	if (!Modernizr.svg){
			//$('.microsite-home img').each(function(){
			$('img').each(function(){
			var imgSrc = $(this).attr("src");
			if (imgSrc.indexOf('svg') >= 0){
				imgSrc = imgSrc.split("svg")
				//console.log(imgSrc[0], imgSrc[1]);
				$(this).attr("src", imgSrc[0] + "png")
			}
			//console.log(imgSrc);
		});
	}
});



// ----- javascript to make the jump menu work ----- //
// ----- javascript to make the jump menu work ----- //
	
// i'm not mad keen on this, and should probably re-write it...
function changeLocation(menuObj) {
	var i = menuObj.selectedIndex;
	if(i > 0) {
	window.location = menuObj.options[i].value;
	}
}


// ----- everything else ----- //
// ----- everything else ----- //

$(document).ready(function() {
	

	// ----- mobile jump menu ----- //
	// ----- mobile jump menu ----- //
	
	var jumpUrl = new Array();
	var jumpText = new Array();
	var jumpCount = 0;
	
	//create and insert an option menu with a couple of ready links
	$("<select onChange='javascript:changeLocation(this)'>").appendTo("#site-navigation");
	$("<option class='selector' value=''>Navigation</option>").appendTo("#site-navigation select");

	//grab the href and text from each link in the hidden 'all-children' navigation and chuck them in an array
	$('#all-children a').each(function(){
		jumpUrl.push($(this).attr('href'));
		jumpText.push($(this).text());
	});

	//get rid of the all-children markup
	$('#all-children').remove();

	//count the number of values in each array, then generate the options for the jump menu from the arrays
	while (jumpCount<(jumpText.length))
		{
			$("<option class='selector' value='" + jumpUrl[jumpCount] + "' >" + jumpText[jumpCount] + '</option>').appendTo('#site-navigation select');
			jumpCount++;
		}

	//stick a home link into the jump menu	
	$("<option class='selector' value='http://www.liverpool.ac.uk/csd/'>Computing Services Home</option>").appendTo("#site-navigation select");
	
	//finally, add the ready class so that the menu never displays until the menu has finsihed being built
	$('#site-navigation select').addClass('ready');



	// ----- classes for the staus list on the CSD home page ----- //
	// ----- classes for the staus list on the CSD home page ----- //

	if (document.getElementById('service-status')){
	
		if ($('.red').length != 0){
			$('#status-list').addClass("service-alert");
			var alertItems = $('#status-list li:not(.green)').clone();
			var alertMessage = $('<ul id="service-alert" class="clearfix">');
			$(alertItems).appendTo(alertMessage);
			$(alertMessage).prependTo('#computing-services-home');
		} else if ($('.amber').length != 0) {
			$('#status-list').addClass("service-alert");
			var alertItems = $('#status-list li:not(.green)').clone();
		} else {
			$('#status-list').addClass("service-ok");
			$('<li>All services are running normally.</li>').prependTo('#status-list');
		}
	
	}


	
	/* ----- for small screen devices that have not chosen the desktop view re-order the page a litte ------ */
	/* ----- for small screen devices that have not chosen the desktop view re-order the page a litte ------ */
	
	if ((screen.width < 700) && (localStorage.getItem('screenKey') !=="non-responsive")) {
		$("#promo-spot, #promo-spot-hang-line").insertBefore("#services");
		$(".microsite-home .sidebar").insertBefore(".content");
		$("#search input[type=submit]").attr("value", "Go!");
		$("#breadcrumb").insertAfter("#masthead");
		$("#services .call-to-action").insertAfter("#support .call-to-action");
	}
	
	$("#search input[type=submit]").addClass("ready");


	/* ----- check if people actually want the responsive version ------ */
	/* ----- check if people actually want the responsive version ------
	
	// declare the variables for local storage
	var screenKey;
	var screenValue;
	
	// if the screen width is less that 700px, and there is no preference already stored...
	if ((screen.width < 700) && (localStorage.getItem('screenKey') === "null")){
			
		// insert the code to choose desktop or mobile picker, then slide it down...
		$("<div id='chooser'><p>Hi there, you look like a smartphone user. Which site would you prefer?</p><ul class='clearfix'><li><a href='#' class='call-to-action'>Desktop</a></li><li><a href='#' class='call-to-action float-right'>Mobile</a></li></ul><p>(you can always change your mind)</p></div>").insertBefore("#masthead");	
		$("#chooser").slideDown("slow");
			
		// if someone asks for the desktop version			
		$("#chooser a:first").click(function(){
			screenValue = "non-responsive";
			localStorage.setItem('screenKey', screenValue);
			$("#chooser").slideUp("slow", function(){
				$("body").removeClass("responsive");
				$("#promo-spot, #promo-spot-hang-line").insertBefore("#quick-links");
				$(".microsite-home .sidebar").insertAfter(".content");
				$('meta[name=viewport]').attr('content', 'width=1001,initial-scale=0.5');
			});
		});
		
		// if someone asks for the mobile version
		$("#chooser a:last").click(function(){
			screenValue = "responsive";
			localStorage.setItem('screenKey', screenValue);
			$("#chooser").slideUp("slow", function(){
				$("body").addClass("responsive");
				$('meta[name=viewport]').attr('content', 'width=device-width,initial-scale=1');
			});
		});
	
	}
	
	// if someone has chosen desktop version previously give it to them
	if((screen.width < 700) && (localStorage.getItem('screenKey') == "non-responsive" )){
		$("body").removeClass("responsive");
		$('meta[name=viewport]').attr('content', 'width=1001,initial-scale=0.5');
		$("<a id='reset' href='#' class='call-to-action large-desktop-link'>Visit mobile site</a><hr>").insertAfter("footer");
		$("#reset").click(function(){
			screenValue = "responsive";
			localStorage.setItem('screenKey', screenValue);
			location.reload(true);
		});
	}
	
	else if ((screen.width < 700) && (localStorage.getItem('screenKey') !== "non-responsive" )){
		$("<a id='reset' href='#' class='call-to-action'>Visit desktop site</a><hr>").insertAfter("footer");
		$("#reset").click(function(){
			screenValue = "non-responsive";
			localStorage.setItem('screenKey', screenValue);
			location.reload(true);
		});
	}
	 */


	/* ----- strip out inline CSS values for width and height on images ------ */
	/* ----- strip out inline CSS values for width and height on images ------ */
	
	$('#main-content img').attr('style','');


	/* ----- search selector, CSD + desktop only ----- */
	/* ----- search selector, CSD + desktop only ----- */


	if (document.URL.indexOf('/csd') !== -1) {
		
				
		/* on page load default the searh to computing services */
		if (screen.width > 700){
			$('#search input[name=collection]').attr('value','csd');
		}
	
		// generate the search select div and radio buttons
		$('<div class="search-select"><span>Search within:</span><br><label><input name="search-select" type="radio" id="search-csd" value="radio" checked ><span>Computing Services</span></label><br><label><input type="radio" name="search-select" value="radio" id="search-liv"><span>University of Liverpool website</span></label></div>').appendTo('#search');
		$('.search-input').focus(function() {
			if (screen.width > 700){
				$('div.search-select').slideDown('medium');
				$(document).bind('focusin.search-select click.search-select',function(e) {
					if ($(e.target).closest('.search-select, .search-input').length) return;
					$(document).unbind('.search-select');
					$('div.search-select').slideUp('medium');
				});
			}
		});
		$('div.search-select').hide();
		
		//when a user selects either CSD or the whole site update the search form properties
		$('.search-select #search-csd').click(function(){
			$('#search input[name=collection]').attr('value','csd');
		});
		$('.search-select #search-liv').click(function(){
			$('#search input[name=collection]').attr('value','main-collection');
		});
		
	}


	// ----- auto generate an a-to-z jump menu ----- */
	// ----- auto generate an a-to-z jump menu ----- */

	if (document.getElementById('a-to-z-list-with-sidebar')){
	
		/* add an anchor link to each h2 element in the main content area*/
		$('.content h2').each(function(){
			var idText = $(this).text();
			$(this).attr('id', idText);
		});
		
		/* create a <ul> and a bunch of <li>	 */
		var sideBar = $('<aside id="a-to-z-aside"></aside>');
		var azLinkMenu = $('<ul id="a-to-z-links"></ul>');
		var listItems = new Array();
		listItems = $('.content h2').text();
		$.each(
			listItems,
			function(intIndex, objValue){
				azLinkMenu.append(
					$( '<li><a href="#' + objValue + '">' + objValue + '</a></li>' )
				);
			}
		);
		$('<h1>Jump to...</h1>').appendTo(sideBar);
		$(azLinkMenu).appendTo(sideBar);
		$('<a class="call-to-action" href="#computing-services">back to top...</a>').appendTo(sideBar);
		$(sideBar).insertAfter('.content');
		
		/* do a nice smooth scroll to each id */
			
		$('aside a').click(function(event) {
			event.preventDefault();
			var target = $(this).attr('href');
			var targetOffset = ($(target).offset().top) - 40;	
			$('html,body').animate({scrollTop: targetOffset}, {duration:800,easing:'swing'});		
		});
		
		var windowWidth = $(window).width();
		console.log(windowWidth);
		 $(window).resize(function() {
			 windowWidth = $(window).width();
			 if ( windowWidth < 1000) {
				$("aside").css({ "position": "fixed", "top": "auto", "right": "0" }); //same here
			 } else {
				$("aside").css({ "position": "absolute", "top": "150px", "right": "5%" }); //same here
			 }
		 });			
	
		
		$(window).scroll(function() {
			if ( windowWidth > 1000) {	/* fix the position of the sidebar */
				if ($(this).scrollTop() > 150) { //I just used 200 for testing
					$("aside").css({ "position": "fixed", "top": 0 });
				} else {
					$("aside").css({ "position": "absolute", "top": "150px", "right": "5%" }); //same here
				}   			
			}
			else{
			}
		});
	}

});



// ----- simple tabbed panels ----- //
// ----- simple tabbed panels ----- //

if (document.getElementsByClassName('tabcontent')){
	
	var showHide;
	var linkCounter = 1;
	var selectedTab;
	var tabsID = $('.basic-tabs').attr('id');
	var storedTab = localStorage.getItem(tabsID + 'Tabs', showHide);
	var tabText;
	var tabsList = $('<ul>');
	var tabsWrapper = $('<div class="basic-tabs">');
	//var tabsNav = $('<nav>');
	
	/* wrap up all the tabs in a wrapper */
	$('.tabcontent').wrapAll(tabsWrapper);
		
	/* create the tab menu */
	$('.tabcontent').each(function(){
		tabText = $(this).attr('data-tabname');
		$('<li><a href="#">' + tabText + '</a></li>').appendTo(tabsList);
	});
	
	$(tabsList).prependTo('.basic-tabs');
	$('.basic-tabs ul:first').wrap('<nav>');
		
	/* add an id to each tab */
	$('.basic-tabs .tabcontent').each(function(){
		$(this).attr('id', 'tab-' + linkCounter);
		linkCounter ++;
	});
	
	/* reset the counter */
	
	var linkCounter = 1;
	
	$('.basic-tabs nav a').each(function(){
		$(this).attr('href', '#tab-' + linkCounter);
		linkCounter ++;
	});
	
	
	/* reset all the tabs */
	
	function tabHide(){
		$('.basic-tabs .tabcontent').addClass('visuallyhidden');
		$('.basic-tabs nav a').removeClass('selected');
	}
	
	/* hide all the tabs and clear the selected class on the list items */
	
	tabHide();
	
	if (storedTab == null){
		$('.basic-tabs nav a:first').addClass('selected');
		$('.basic-tabs .tabcontent:first').removeClass('visuallyhidden');
	} else{
		$('.basic-tabs nav a[href=' + storedTab + ']').addClass('selected');
		$('.basic-tabs ' + storedTab ).removeClass('visuallyhidden');
	}
	
	
	// when there's  a click on a tab, show it, and set a local storage value
	$('.basic-tabs nav a').live('click', function(event){
		event.preventDefault();
		showHide=$(this).attr('href');
		tabHide();
		$(this).addClass('selected');
		$('.basic-tabs ' + showHide).removeClass('visuallyhidden')
		localStorage.setItem(tabsID + 'Tabs', showHide);
	});
	
}


// ----- simple accordion panels ----- //
// ----- simple accordion panels ----- //

if (document.getElementsByClassName('.ShowHideContent')){
	
	//hide all apart from the first
	$('.ShowHideContent:gt(0) > *').not('.show-hide-trigger').hide();
	
	$('.show-hide-trigger:eq(0)').addClass('close');
	$('.show-hide-trigger:gt(0)').addClass('open');
	
	$('.show-hide-trigger').click(function(event){
		event.preventDefault();
		if ($(this).hasClass('close')){
			$(this).parent().children().not('.show-hide-trigger').slideUp();
			$(this).removeClass('close');
			$(this).addClass('open');
		} else if ($(this).hasClass('open')){
			$(this).parent().children().not('.show-hide-trigger').slideDown();
			$(this).removeClass('open');
			$(this).addClass('close');
		}
	});
	
}



